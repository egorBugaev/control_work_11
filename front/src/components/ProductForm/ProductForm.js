import React, {Component} from 'react';
import {Button, Col, ControlLabel, DropdownButton, Form, FormControl, FormGroup, MenuItem} from "react-bootstrap";

class ProductForm extends Component {
  state = {
    title: '',
    description: '',
    image: '',
    price:'',
    category:''
  };

  submitFormHandler = event => {
    event.preventDefault();
      const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
      this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };
    categoryChange = event=>{
      this.setState({
          category: event,
          user:this.props.user._id
      })
    };



  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        <FormGroup controlId="producttName">
          <Col componentClass={ControlLabel} sm={2}>
            Name
          </Col>
          <Col sm={10}>
            <FormControl
              type="text" required
              placeholder="Enter product name"
              name="title"
              value={this.state.title}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="productDescription">
          <Col componentClass={ControlLabel} sm={2}>
            Description
          </Col>
          <Col sm={10}>
            <FormControl
              componentClass="textarea" required
              placeholder="Enter description"
              name="description"
              value={this.state.description}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>
       <FormGroup controlId="productPrice">
              <Col componentClass={ControlLabel} sm={2}>
                Description
              </Col>
              <Col sm={10}>
                <FormControl
                  type="number" required
                  placeholder="Enter price"
                  name="price"
                  value={this.state.price}
                  onChange={this.inputChangeHandler}
                />
              </Col>
            </FormGroup>

        <FormGroup controlId="productImage">
          <Col componentClass={ControlLabel} sm={2}>
            Image
          </Col>
          <Col sm={10}>
            <FormControl
              type="file"
              name="image"
              onChange={this.fileChangeHandler}
            />
          </Col>
        </FormGroup>
          <FormGroup>
              <DropdownButton
                  id={'id'}
                  title={'Category'}>
                  {this.props.categories.map(item=>(
                      <MenuItem key={item._id} eventKey={item._id} onSelect={this.categoryChange}>{item.title}</MenuItem>

                  ))}
              </DropdownButton>
          </FormGroup>


        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default ProductForm;
