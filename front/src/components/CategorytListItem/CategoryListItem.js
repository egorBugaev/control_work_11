import React from 'react';
import {Nav} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';



const CategoryListItem = props => {
	return (
		<Nav>

				<Link to={'/categories/' + props.id}>
					{props.title}
				</Link>

		</Nav>
	);
};

CategoryListItem.propTypes = {
	id: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,

};

export default CategoryListItem;