import React from 'react';
import {Nav, Navbar} from "react-bootstrap";


import AnonymousMenu from "./Menus/AnonymousMenu";
import UserMenu from "./Menus/UserMenu";
import Category from "../../../containers/Category/Category";
import {LinkContainer} from "react-router-bootstrap";


const Toolbar = ({user, logout}) => (
	<Navbar>
		<Navbar.Header>
			 <Navbar.Brand>
            <LinkContainer to="/" exact><a>Flea Market</a></LinkContainer>
        </Navbar.Brand>
			<Navbar.Toggle />
		</Navbar.Header>
		<Navbar.Collapse>
			<Nav>
				<Category/>
			</Nav>
			{user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu />}
		</Navbar.Collapse>
	</Navbar>
);

export default Toolbar;