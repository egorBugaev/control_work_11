import axios from '../../axios-api';
import { FETCH_CATEGORY_SUCCESS} from "./actionTypes";

export const fetchCategoriesSuccess = category => {
	return {type: FETCH_CATEGORY_SUCCESS, category: category};
};


export const fetchCategory = () => {
	return dispatch => {
		axios.get('/categories').then(
			response => {

                dispatch(fetchCategoriesSuccess(response.data))
            }
		);
	}
};
