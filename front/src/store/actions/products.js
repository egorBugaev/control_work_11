import axios from '../../axios-api';
import {FETCH_PRODUCT_SUCCESS, CREATE_PRODUCT_SUCCESS} from "./actionTypes";

export const fetchProductSuccess = product => {
    return {type: FETCH_PRODUCT_SUCCESS, product: product};
};

export const fetchItemsSuccess = product => {
    return {type:FETCH_PRODUCT_SUCCESS,  product: product};
};

export const fetchItems = () => {
    return dispatch => {
        axios.get('/products').then(
            response => {
                dispatch(fetchProductSuccess(response.data))
            }
        );
    }
};
//
// export const fetchTracks = (id) => {
//     return dispatch => {
//         axios.get(id).then(
//             response => dispatch(fetchTrackSuccess(response.data))
//         );
//     }
// };
//
// export const fetchArtist = (id) => {
//     return dispatch => {
//         axios.get(id).then(
//             response => dispatch(fetchArtistIDSuccess(response.data))
//         );
//     }
// };
export const fetchItemsId = (id) => {
    return dispatch => {
        axios.get(`/products/${id}`).then(
            response => dispatch(fetchItemsSuccess(response.data))
        );
    }
};
export const fetchItemsIdByCategory = (id) => {
    return dispatch => {
        axios.get(`/categories/${id}`).then(
            response => dispatch(fetchItemsSuccess(response.data))
        );
    }
};

export const createProductSuccess = () => {
    return {type: CREATE_PRODUCT_SUCCESS};
};

export const createProduct = artistData => {
    return dispatch => {
        return axios.post('/products', artistData).then(
            response => dispatch(createProductSuccess())
        );
    };
};
// export const createAlbum = artistData => {
//     return dispatch => {
//         return axios.post('/albums', artistData).then(
//             response => dispatch(createArtistSuccess())
//         );
//     };
// };