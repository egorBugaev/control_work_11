import {
    FETCH_PRODUCT_SUCCESS,

} from "../actions/actionTypes";

const initialState = {
    product: [],

};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_PRODUCT_SUCCESS:
            return {...state, product: action.product};
        // case FETCH_ALBUM_SUCCESS:
        //     return {...state, album: action.album};
        // case FETCH_TRACK_SUCCESS:
        //     return {...state, tracks: action.tracks};
        // case FETCH_ARTIST_ID_SUCCESS:
        //     return {...state, artist: action.artist};
        default:
            return state;
    }
};

export default reducer;