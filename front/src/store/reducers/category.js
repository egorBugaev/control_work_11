import {
    FETCH_CATEGORY_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  category: [],

};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_CATEGORY_SUCCESS:
	    return {...state, category: action.category};
    default:
      return state;
  }
};

export default reducer;