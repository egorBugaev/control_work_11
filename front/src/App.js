import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Products from "./containers/Products/Products";
import Items from "./containers/Items/items";
import Register from "./containers/Register/Register";
import Layout from "./containers/Layout/Layout";
import Login from "./containers/Login/Login";
import ProductAdd from "./containers/ProductAdd/ProductAdd";

class App extends Component {
	render() {
		return (
			<Layout>
				<Switch>
					<Route path="/" exact component={Products}/>
					<Route path="/item/:id" component={Items}/>
					<Route path="/categories/:id" component={Items}/>
					<Route path="/products/new" component={ProductAdd}/>
					<Route path="/register" exact component={Register}/>
					<Route path="/login" exact component={Login}/>
				</Switch>
			</Layout>
		);
	}
}

export default App;
