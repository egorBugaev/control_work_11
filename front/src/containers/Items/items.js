import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ProductListItem from '../../components/ProductListItem/ProductListItem';
import {fetchItemsId, fetchItemsIdByCategory} from "../../store/actions/products";

class Items extends Component {
	 componentDidMount() {
         console.log(this.props);
         if(this.props.match.path === '/categories/:id'){
             this.props.onFetchItemsBycategory(this.props.match.params.id);
         }else{
             this.props.onFetchItems(this.props.match.params.id);
         }
  }

  render() {
    let list= null;
    if(this.props.product){
    	list=this.props.product.map(item => (
		    <ProductListItem
			    key={item._id}
			    id={item._id}
			    title={item.title}
			    image={item.image}
                price={item.price}

		    />
	    ))
    }

	  return (
      <Fragment>
        <PageHeader>
         Product
        </PageHeader>
        {list}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
    return {
    product: state.products.product
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchItems: (id) => dispatch(fetchItemsId(id)),
    onFetchItemsBycategory: (id) => dispatch(fetchItemsIdByCategory(id)),

  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Items);