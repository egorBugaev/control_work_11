import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ProductListItem from '../../components/ProductListItem/ProductListItem';
import {fetchItems} from "../../store/actions/products";

class Products extends Component {
	 componentDidMount() {
	 	this.props.onFetchProduct();
  }

  render() {
    let list= null;
    if(this.props.product){
    	list=this.props.product.map(item => (
		    <ProductListItem
			    key={item._id}
			    id={item._id}
			    title={item.title}
			    image={item.image}
                price={item.price}
		    />
	    ))
    }

	  return (
      <Fragment>
        <PageHeader>
         Product
        </PageHeader>
        {list}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
    return {
    product: state.products.product
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchProduct: () => dispatch(fetchItems()),

  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);