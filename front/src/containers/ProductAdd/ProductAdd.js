import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ProductForm from "../../components/ProductForm/ProductForm";
import {createProduct} from "../../store/actions/products";
import {fetchCategory} from "../../store/actions/category";

class ProductAdd extends Component {
    componentDidMount() {
        this.props.onFetchCategory();
    }
	createProduct = producdData => {
		this.props.onProductCreated(producdData).then(() => {
			this.props.history.push('/');
		});
	};

	render() {

		return (
			<Fragment>
				<PageHeader>New Product</PageHeader>
				<ProductForm
					user={this.props.user}
                    categories={this.props.categories}
					onSubmit={this.createProduct}/>
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
    return {
        categories: state.category.category,
		user: state.users.user
    }
};


const mapDispatchToProps = dispatch => {
	return {
		onProductCreated: productData => dispatch(createProduct(productData)),
        onFetchCategory: () => dispatch(fetchCategory())
	}
};


export default connect(mapStateToProps, mapDispatchToProps)(ProductAdd);