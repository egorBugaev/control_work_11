import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchCategory} from "../../store/actions/category";


import CategoryListItem from '../../components/CategorytListItem/CategoryListItem';

class Category extends Component {
  componentDidMount() {
    this.props.onFetchCategory();
  }

  render() {
    return (
      <Fragment>

        {this.props.categories.map(item => (
          <CategoryListItem
            key={item._id}
            id={item._id}
            title={item.title}
          />
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
	return {
   categories: state.category.category
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchCategory: () => dispatch(fetchCategory())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Category);