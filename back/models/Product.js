const mongoose = require('mongoose');
const Schema =  mongoose.Schema;

const ProductSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    description:{
        type: String,
        required: true
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    },
    price: {
        type: Number, required: true
    },
    image: String,
    user:{
        type: Schema.Types.ObjectId,
        ref: 'Users',
        required: true
    }


});

const Product = mongoose.model('Products', ProductSchema);

module.exports = Product;