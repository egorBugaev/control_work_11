const mongoose = require('mongoose');
const config = require('./config');

const Category = require('./models/Category');
const Product = require('./models/Product');
const User = require('./models/User');
const {hdd, cpu, ram, greese, screw} = require('./fixtures_image');


mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('categories');
    await db.dropCollection('products');
    await db.dropCollection('users');

  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [cpuCategory, hddCategory, ramCategory, toolsCategory] = await Category.create({
    title: 'CPUs',
    description: 'Central Processor Units'
  }, {
    title: 'HDDs',
    description: 'Hard Disk Drives'
  }, {
    title: 'RAM',
    description: 'Random Access Memory'
  }, {
    title: 'Tools',
    description: 'PC Tools'
  });

  await Product.create({
    title: 'Intel Core i7',
    price: 300,
    description: 'Very cool processor',
    category: cpuCategory._id,
    image: cpu
  }, {
    title: 'Seagate 3TB',
    price: 110,
      description: 'Some kinda description',
    category: hddCategory._id,
    image: hdd
  },{
    title: 'Infenion DDR4 16GB',
    price: 110,
    description: 'Some kinda description',
    category: ramCategory._id,
    image: ram
  },{
    title: 'Thermal grease',
    price: 110,
      description: 'Some kinda description',
    category: toolsCategory._id,
    image: greese
  },{
    title: 'Screwdriver',
    price: 110,
    description: 'Some kinda description',
    category: toolsCategory._id,
    image: screw
  });

  db.close();
});