const express = require('express');
const multer = require('multer');
const Product = require('../models/Product');

const upload = multer({ storage: multer.memoryStorage({}) });

const router = express.Router();

const createRouter = (db) => {
    // Product index
    router.get('/', (req, res) => {
        Product.find().populate('category')
            .then(results =>res.send(results))
            .catch(() => res.sendStatus(500));
    });
    // Product create
    router.post('/', upload.single('image'), (req, res) => {
        const productData = req.body;
	    if (req.file) {
		    productData.image = new Buffer(req.file.buffer).toString("base64");
        } else {
            productData.image = null;
        }

        const product = new Product(productData);

        product.save()
            .then(result => res.send(result))
            .catch(error => {
                console.log(error);
                res.status(400).send(error)
            });
    });
	router.get('/:id', (req, res) => {
		Product.find().populate('category')
			.find({_id: req.params.id})
			.then(result => {
				if (result) res.send(result);
				else res.sendStatus(404);
			})
			.catch(() => res.sendStatus(500));
	});

    return router;
};

module.exports = createRouter;