const express = require('express');
const multer = require('multer');

const Category = require('../models/Category');
const Product = require('../models/Product');

const upload = multer({ storage: multer.memoryStorage({}) });

const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    Category.find()
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  router.post('/', upload.single('image'), (req, res) => {
    const albumData = req.body;

    if (req.file) {
	    albumData.image = new Buffer(req.file.buffer).toString("base64");
    } else {
      albumData.image = null;
    }

    const album = new Category(albumData);

    album.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/:id', (req, res) => {
	  Product.find()
      .find({category: req.params.id})
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  return router;
};

module.exports = createRouter;